skinparam classFontName SegoeUI
scale 10

@startuml

class CreationParcelle
{
	- taille_Carte : const int = 10
	- {static} CasesParcelles : List<Case>
	__
	.. Methodes de manipulation ..
	+ {static} CreerParcelles(tab_Case : Case[,]) : List<Parcelle>
	+ {static} NouvelleParcelle(tab_Case : Case[,]) : Parcelle
	+ {static} AjouterCase(tab_Case : Case[,], indiceLigne, indiceColonne, id_Parcelle : int) : void
	.. Methodes de verification ..
	+ {static} VerifNord(tab_Case : Case[,], indiceLigne, indiceColonne : int) : bool
        + {static} VerifEst(tab_Case : Case[,], indiceLigne, indiceColonne : int) : bool
        + {static} VerifSud(tab_Case : Case[,], indiceLigne, indiceColonne : int) : bool
        + {static} VerifOuest(tab_Case : Case[,], indiceLigne, indiceColonne : int) : bool
}

class IntelligenceArtificielle
{
	.. Constante ..
	- TAILLE_CARTE : const in = 10
	.. Attributs ..
	- {static} m_casesJouables : List<int[]>
	- {static} m_casesJouablesAdjacentes : List<int[]>
	- {static} m_casesChaines : Dictionary<int[], int>
	.. Proprietes ..
	+ {static} CasesJouables : List<int[]>
	+ {static} CasesJouablesAdjacentes : List<int[]>
	+ {static} CasesChaines : Dictionary<int[], int>
	__
	+ {static} Jouer(p_carte : Carte, p_premier_tour : bool = false) : int[]
	+ {static} GetCasesJouables(p_carte : Carte, coordonneesDernierePose : int[]) : void
	+ {static} GetCasesJouablesPremierTour(p_carte : Carte) : void
	+ {static} CaseIdeale(p_carte : Carte) : int[]
	+ {static} PremierTour(p_carte : Carte) : int[]
	+ {static} PlacementStrategique(p_carte : Carte) : int[]
	+ {static} ZeroGraineAdjacente(p_carte : Carte) : int[]
	+ {static} MeilleureParcelle(p_parcelles : List<Parcelle>) : int
	+ {static} GetCasesJouablesAdjacentes(p_carte : Carte) : void
	+ {static} ComptageChaines(p_carte : Carte) : void
	+ {static} AjouterChaine(p_indiceLigne : int, p_indiceColonne : int, p_casesAllieesNonComptees : List<int[]>, p_caseJouable : int[]) : void
	+ {static} VerifNord(p_indiceLigne : int, p_indiceColonne : int, p_casesAllieesNonComptees : List<int[]>) : bool
	+ {static} VerifEst(p_indiceLigne : int, p_indiceColonne : int, p_casesAllieesNonComptees : List<int[]>) : bool
	+ {static} VerifSud(p_indiceLigne : int, p_indiceColonne : int, p_casesAllieesNonComptees : List<int[]>) : bool
	+ {static} VerifOuest(p_indiceLigne : int, p_indiceColonne : int, p_casesAllieesNonComptees : List<int[]>) : bool
	+ {static} ChoixPlusieursPlacement(p_carte : Carte) : int[]
	+ {static} ChoixControle(p_meilleurPlacement : List<int[]>, p_carte : Carte) : int[]
	+ {static} ChoixVoisinnage(p_meilleurPlacement : List<int[]>, p_carte : Carte) : int[]
	.. Getter ..
	+ {static} GetCasesVoisinesLibres(p_coords : int[], p_carte : Carte) : int
}

@enduml
