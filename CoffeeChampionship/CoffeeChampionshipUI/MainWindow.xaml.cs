﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoffeeChampionshipEngine;

namespace CoffeeChampionshipUI
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CoffeeGrid_Loaded(object sender, RoutedEventArgs e)
        {
            Client coffee = new Client();
            Case[,] cases = coffee.GetCarte().GetCases();

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    String terrain = "", nord = "", sud = "", est = "", ouest = "";

                    switch (cases[j,i].getTerrain())
                    {
                        case Case.Terrain.Neutre:
                            terrain = "Neutre";
                            break;
                        case Case.Terrain.Mer:
                            terrain = "Mer";
                            break;
                        case Case.Terrain.Foret:
                            terrain = "Foret";
                            break;
                    }

                    if (cases[j, i].getNord())
                        nord = "nord";
                    if (cases[j, i].getSud())
                        sud = "sud";
                    if (cases[j, i].getEst())
                        est = "est";
                    if (cases[j, i].getOuest())
                        ouest = "ouest";

                    string debutChemin = "pack://application:,,,/";
                    string cheminRelatif = System.IO.Path.GetFullPath(System.IO.Path.Combine(debutChemin, @"..\..\..\..\..\..\"));
                    string cheminFichier = String.Format(@"imgs\tiles\{0}\{1}\{2}{3}{4}{5}.png", terrain, "neutre", nord, sud, est, ouest);

                    Image img = new Image() { Width = 73.5, Height = 73.5 };
                    BitmapImage bitmapImage = new BitmapImage(new Uri(String.Format("{0}{1}", cheminRelatif, cheminFichier)));
                    img.Source = bitmapImage;
                    img.SetValue(Grid.RowProperty, j);
                    img.SetValue(Grid.ColumnProperty, i);
                    CoffeeGrid.Children.Add(img);
                }
            }

        }
    }
}
