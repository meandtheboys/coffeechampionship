﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoffeeChampionshipEngine;

namespace CoffeeChampionshipEngineTests
{
    [TestClass]
    public class TestCarte
    {
        string map1 = "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44";
        int[,] terrain = new int[10, 10]
        {
                {
                    3,
                    9,
                    71,
                    69,
                    65,
                    65,
                    65,
                    65,
                    65,
                    73
                },
                {
                    2,
                    8,
                    3,
                    9,
                    70,
                    68,
                    64,
                    64,
                    64,
                    72
                },
                {
                    6,
                    12,
                    2,
                    8,
                    3,
                    9,
                    70,
                    68,
                    64,
                    72
                },
                {
                    11,
                    11,
                    6,
                    12,
                    6,
                    12,
                    3,
                    9,
                    70,
                    76
                },
                {
                    10,
                    10,
                    11,
                    11,
                    67,
                    73,
                    6,
                    12,
                    3,
                    9
                },
                {
                    14,
                    14,
                    10,
                    10,
                    70,
                    76,
                    7,
                    13,
                    6,
                    12
                },
                {
                    3,
                    9,
                    14,
                    14,
                    11,
                    7,
                    13,
                    3,
                    9,
                    75
                },
                {
                    2,
                    8,
                    7,
                    13,
                    14,
                    3,
                    9,
                    6,
                    12,
                    78
                },
                {
                    6,
                    12,
                    3,
                    1,
                    9,
                    6,
                    12,
                    35,
                    33,
                    41
                },
                {
                    71,
                    77,
                    6,
                    4,
                    12,
                    39,
                    37,
                    36,
                    36,
                    44
                }
  };
        [TestMethod]
        public void TestFrontiere()
        {
            Assert.IsTrue(Carte.Frontiere(1).getNord());
            Assert.IsTrue(Carte.Frontiere(8).getEst());
            Assert.IsTrue(Carte.Frontiere(2).getOuest());
            Assert.IsTrue(Carte.Frontiere(4).getSud());
            Assert.IsTrue(Carte.Frontiere(15).getNord());
            Assert.IsTrue(Carte.Frontiere(15).getSud());
            Assert.IsTrue(Carte.Frontiere(15).getOuest());
            Assert.IsTrue(Carte.Frontiere(15).getEst());
            Assert.IsFalse(Carte.Frontiere(0).getNord());
            Assert.IsFalse(Carte.Frontiere(0).getSud());
            Assert.IsFalse(Carte.Frontiere(0).getEst());
            Assert.IsFalse(Carte.Frontiere(0).getOuest());
        }

        [TestMethod]
        public void TestConvertionIntEnCase()
        {
            Case[,] terrainCase = new Case[10, 10];
            terrainCase = Carte.ConvertionIntEnCase(terrain);
            Assert.AreEqual(terrainCase[0, 0].getTerrain(), Case.Terrain.Neutre);
            Assert.IsTrue(terrainCase[0, 0].getNord());
            Assert.IsTrue(terrainCase[0, 0].getOuest());
            Assert.AreEqual(terrainCase[0, 3].getTerrain(), Case.Terrain.Mer);
            Assert.IsTrue(terrainCase[0, 3].getNord());
            Assert.IsTrue(terrainCase[0, 3].getSud());
            Assert.AreEqual(terrainCase[8, 7].getTerrain(), Case.Terrain.Foret);
            Assert.IsTrue(terrainCase[8, 7].getNord());
        }

        [TestMethod]
        public void TestTraduire()
        {
            Case[,] terrainCase = new Case[10, 10];
            terrainCase = Carte.Traduire(map1);
            Assert.AreEqual(terrainCase[0, 0].getTerrain(), Case.Terrain.Neutre);
            Assert.IsTrue(terrainCase[0, 0].getNord());
            Assert.IsTrue(terrainCase[0, 0].getOuest());
            Assert.AreEqual(terrainCase[0, 3].getTerrain(), Case.Terrain.Mer);
            Assert.IsTrue(terrainCase[0, 3].getNord());
            Assert.IsTrue(terrainCase[0, 3].getSud());
            Assert.AreEqual(terrainCase[8, 7].getTerrain(), Case.Terrain.Foret);
            Assert.IsTrue(terrainCase[8, 7].getNord());
        }

        [TestMethod]
        public void TestSetAppartennance()
        {
            Carte carte1 = new Carte(map1);
            Assert.AreEqual(carte1.SetCase(0, 0, Case.Appartenance.Ennemi), Case.Appartenance.Ennemi);
            Assert.AreEqual(carte1.SetCase(0, 0, Case.Appartenance.Allie), Case.Appartenance.Allie);
            Assert.AreEqual(carte1.SetCase(0, 0, Case.Appartenance.Personne), Case.Appartenance.Personne);
        }
    }
}
