﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoffeeChampionshipEngine;

namespace CoffeeChampionshipEngineTests
{
    /// <summary>
    /// Description résumée pour TestCreationParcelle
    /// </summary>
    [TestClass]
    public class TestCreationParcelle
    {
        //Définition d'un tableau de Cases de 10 lignes et 10 colonnes pour simuler une carte de jeu et tester les fonctions
        //    _____________________
        //  0 |.|.M.|.|___|F|.|.F_|
        //  1 |.|___|.|.|___|.|.|M|
        //  2 |_|_____|_______|_|_|
        //  3 |.|.M.|.|___|F|.|.F_|
        //  4 |.|___|.|.|___|.|.|M|
        //  5 |_|_____|_______|_|_|
        //  6 |.|.M.|.|___|F|.|.F_|
        //  7 |.|___|.|.|___|.|.|M|
        //  8 |_|_____|_______|_|_|
        //  9 |___________________|
        //     0 1 2 3 4 5 6 7 8 9
        Case[,] Tab_Case = new Case[10, 10]
        {
            {
                //Ligne 0 NORD  EST   OUEST  SUD
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(true, true, false, true),
                new Case(true, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, true)
            },
            {
                //Ligne 1 NORD  EST   OUEST  SUD
                new Case(false, true, true, false),
                new Case(false, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(false, true, true, false),
                new Case(true, true, true, false)
            },
            {
                //Ligne 2 NORD  EST   OUEST  SUD
                new Case(false, true, true, true),
                new Case(true, false, true, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, false, true, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, true, true, true),
                new Case(false, true, true, true)
            },
            {
                //Ligne 3 NORD  EST   OUEST  SUD
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(true, true, false, true),
                new Case(true, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, true)
            },
            {
                //Ligne 4 NORD  EST   OUEST  SUD
                new Case(false, true, true, false),
                new Case(false, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(false, true, true, false),
                new Case(true, true, true, false)
            },
            {
                //Ligne 5 NORD  EST   OUEST  SUD
                new Case(false, true, true, true),
                new Case(true, false, true, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, false, true, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, true, true, true),
                new Case(false, true, true, true)
            },
            {
                //Ligne 6 NORD  EST   OUEST  SUD
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(true, true, false, true),
                new Case(true, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, false),
                new Case(true, true, false, true)
            },
            {
                //Ligne 7 NORD  EST   OUEST  SUD
                new Case(false, true, true, false),
                new Case(false, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(true, true, true, false),
                new Case(true, false, true, true),
                new Case(false, true, false, true),
                new Case(false, true, true, false),
                new Case(false, true, true, false),
                new Case(true, true, true, false)
            },
            {
                //Ligne 8 NORD  EST   OUEST  SUD
                new Case(false, true, true, true),
                new Case(true, false, true, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, false, true, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(false, true, false, true),
                new Case(false, true, true, true),
                new Case(false, true, true, true)
            },
            {
                //Ligne 9 NORD  EST   OUEST  SUD
                new Case(true, false, true, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, false, false, true),
                new Case(true, true, false, true)
            }
        };

        [TestMethod]
        public void TestVerifNord()
        {
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 0, 0), false);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 9, 9), false);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 1, 0), true);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 8, 7), true);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 2, 2), false);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 2, 3), true);
            Tab_Case[1, 3].setIdParcelle(1);
            Assert.AreEqual(CreationParcelle.VerifNord(Tab_Case, 2, 3), false);
        }

        [TestMethod]
        public void TestVerifEst()
        {
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 0, 0), false);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 9, 9), false);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 2, 4), true);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 5, 6), true);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 4, 4), false);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 5, 4), true);
            Tab_Case[5, 5].setIdParcelle(1);
            Assert.AreEqual(CreationParcelle.VerifEst(Tab_Case, 5, 4), false);
        }

        [TestMethod]
        public void TestVerifSud()
        {
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 0, 0), true);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 9, 9), false);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 4, 3), true);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 6, 4), false);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 7, 6), false);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 7, 7), true);
            Tab_Case[8, 7].setIdParcelle(1);
            Assert.AreEqual(CreationParcelle.VerifSud(Tab_Case, 7, 7), false);
        }

        [TestMethod]
        public void TestVerifOuest()
        {
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 0, 0), false);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 9, 9), true);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 8, 7), true);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 5, 4), false);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 1, 7), false);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 2, 7), true);
            Tab_Case[2, 6].setIdParcelle(1);
            Assert.AreEqual(CreationParcelle.VerifOuest(Tab_Case, 2, 7), false);
        }

        [TestMethod]
        public void TestAjouterCase()
        {
            Assert.AreEqual(Tab_Case[0, 0].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[1, 0].getIdParcelle(), -1);
            CreationParcelle.AjouterCase(Tab_Case, 0, 0, 0);
            Assert.AreEqual(Tab_Case[0, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[1, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[2, 0].getIdParcelle(), 0);


            CreationParcelle.AjouterCase(Tab_Case, 2, 5, 1);
            Assert.AreEqual(Tab_Case[0, 7].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[1, 7].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[2, 7].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[1, 4].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[2, 4].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[2, 5].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[2, 6].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[3, 5].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[0, 4].getIdParcelle(), -1);

            CreationParcelle.AjouterCase(Tab_Case, 6, 3, 5);
            Assert.AreEqual(Tab_Case[6, 3].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[7, 3].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[8, 1].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[8, 2].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[8, 3].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[7, 2].getIdParcelle(), -1);
        }

        [TestMethod]
        public void TestNouvelleParcelle()
        {
            Parcelle p0 = CreationParcelle.NouvelleParcelle(Tab_Case, 0);
            Parcelle p1 = CreationParcelle.NouvelleParcelle(Tab_Case, 1);
            Parcelle p2 = CreationParcelle.NouvelleParcelle(Tab_Case, 2);
            Parcelle p3 = CreationParcelle.NouvelleParcelle(Tab_Case, 3);
            Parcelle p4 = CreationParcelle.NouvelleParcelle(Tab_Case, 4);
            Parcelle p5 = CreationParcelle.NouvelleParcelle(Tab_Case, 5);
            Parcelle p6 = CreationParcelle.NouvelleParcelle(Tab_Case, 6);
            Parcelle p7 = CreationParcelle.NouvelleParcelle(Tab_Case, 7);

            Assert.AreEqual(p0.getIdParcelle(), 0);
            Assert.AreEqual(p1.getIdParcelle(), 1);
            Assert.AreEqual(p2.getIdParcelle(), 2);
            Assert.AreEqual(p3.getIdParcelle(), 3);
            Assert.AreEqual(p4.getIdParcelle(), 4);
            Assert.AreEqual(p5.getIdParcelle(), 5);
            Assert.AreEqual(p6.getIdParcelle(), 6);
            Assert.AreEqual(p7.getIdParcelle(), 7);

            Assert.AreEqual(p0.getBlocs().Count, 3);
            Assert.AreEqual(p1.getBlocs().Count, 4);
            Assert.AreEqual(p2.getBlocs().Count, 5);
            Assert.AreEqual(p3.getBlocs().Count, 2);
            Assert.AreEqual(p4.getBlocs().Count, 3);
            Assert.AreEqual(p5.getBlocs().Count, 7);
            Assert.AreEqual(p6.getBlocs().Count, 4);
            Assert.AreEqual(p7.getBlocs().Count, 2);

            //    _____________________
            //  0 |.|.M.|.|___|F|.|.F_|
            //  1 |.|___|.|.|___|.|.|M|
            //  2 |_|_____|_______|_|_|
            //  3 |.|.M.|.|___|F|.|.F_|
            //  4 |.|___|.|.|___|.|.|M|
            //  5 |_|_____|_______|_|_|
            //  6 |.|.M.|.|___|F|.|.F_|
            //  7 |.|___|.|.|___|.|.|M|
            //  8 |_|_____|_______|_|_|
            //  9 |___________________|
            //     0 1 2 3 4 5 6 7 8 9

            //test des id_Parcelle des cases de la carte
            Assert.AreEqual(Tab_Case[0, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[0, 1].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[0, 2].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[0, 3].getIdParcelle(), 2);
            Assert.AreEqual(Tab_Case[0, 4].getIdParcelle(), 3);
            Assert.AreEqual(Tab_Case[0, 5].getIdParcelle(), 3);
            Assert.AreEqual(Tab_Case[0, 6].getIdParcelle(), 4);
            Assert.AreEqual(Tab_Case[0, 7].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[0, 8].getIdParcelle(), 6);
            Assert.AreEqual(Tab_Case[0, 9].getIdParcelle(), 6);

            Assert.AreEqual(Tab_Case[1, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[1, 1].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[1, 2].getIdParcelle(), 1);
            Assert.AreEqual(Tab_Case[1, 3].getIdParcelle(), 2);
            Assert.AreEqual(Tab_Case[1, 4].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[1, 5].getIdParcelle(), 4);
            Assert.AreEqual(Tab_Case[1, 6].getIdParcelle(), 4);
            Assert.AreEqual(Tab_Case[1, 7].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[1, 8].getIdParcelle(), 6);
            Assert.AreEqual(Tab_Case[1, 9].getIdParcelle(), 7);

            Assert.AreEqual(Tab_Case[2, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[2, 1].getIdParcelle(), 2);
            Assert.AreEqual(Tab_Case[2, 2].getIdParcelle(), 2);
            Assert.AreEqual(Tab_Case[2, 3].getIdParcelle(), 2);
            Assert.AreEqual(Tab_Case[2, 4].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[2, 5].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[2, 6].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[2, 7].getIdParcelle(), 5);
            Assert.AreEqual(Tab_Case[2, 8].getIdParcelle(), 6);
            Assert.AreEqual(Tab_Case[2, 9].getIdParcelle(), 7);

            Assert.AreEqual(Tab_Case[3, 0].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 1].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 2].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 3].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 4].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 5].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 6].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 7].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 8].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[3, 9].getIdParcelle(), -1);

            Assert.AreEqual(Tab_Case[9, 0].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 1].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 2].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 3].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 4].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 5].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 6].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 7].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 8].getIdParcelle(), -1);
            Assert.AreEqual(Tab_Case[9, 9].getIdParcelle(), -1);
        }

        [TestMethod]
        public void TestCreerParcelles()
        {
            List<Parcelle> parcelles = CoffeeChampionshipEngine.CreationParcelle.CreerParcelles(Tab_Case);

            Assert.AreEqual(Tab_Case[0, 0].getIdParcelle(), 0);
            Assert.AreEqual(Tab_Case[9, 9].getIdParcelle(), 24);
            Assert.AreEqual(Tab_Case[5, 1].getIdParcelle(), 10);
            Assert.AreEqual(Tab_Case[4, 3].getIdParcelle(), 10);
            Assert.AreEqual(Tab_Case[1, 5].getIdParcelle(), 4);
        }
    }
}
