﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoffeeChampionshipEngine;

namespace CoffeeChampionshipEngineTests
{
    [TestClass]
    public class TestIntelligenceArtificielle
    {
        private CoffeeChampionshipEngine.Carte m_carte;
        private List<int[]> m_listeCasesAllieesNonComptees;

        //Trame correspondant à la carte disponible dans \documents\CarteDeTest_IA pour tester les fonctions de placements.
        private string m_data = "3:9:71:69:65:65:65:65:65:73|" +
                                 "2:8:3:9:70:68:64:64:64:72|" +
                                 "6:12:2:8:3:9:70:68:64:72|" +
                                 "11:11:6:12:6:12:3:9:70:76|" +
                                 "10:10:11:11:67:73:6:12:3:9|" +
                                 "14:14:10:10:70:76:7:13:6:12|" +
                                 "3:9:14:14:11:7:13:3:9:75|" +
                                 "2:8:7:13:14:3:9:6:12:78|" +
                                 "6:12:3:1:9:6:12:35:33:41|" +
                                 "71:77:6:4:12:39:37:36:36:44|";

        //Instanciation de la carte correpondante.
        public TestIntelligenceArtificielle()
        {
            m_carte = new CoffeeChampionshipEngine.Carte(m_data);
            m_listeCasesAllieesNonComptees = new List<int[]>();
            m_listeCasesAllieesNonComptees.Add(new int[] { 2, 2 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 2, 3 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 3, 2 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 6, 4 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 7, 4 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 7, 5 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 7, 6 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 8, 5 });
            m_listeCasesAllieesNonComptees.Add(new int[] { 8, 6 });
            m_carte.GetCases()[2, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[2, 3].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[3, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[6, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[8, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[8, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestGetCasesJouables()
        {

            m_carte.GetCases()[2, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[2, 3].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[3, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[6, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);

            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 0, 0 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 6);

            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 2, 4 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 8);

            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 8, 9 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 9);
        }

        [TestMethod]
        public void TestGetCasesJouablesPremierTour()
        {
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouablesPremierTour(m_carte);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 64);
        }

        [TestMethod]
        public void TestMeilleureParcelle()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            parcelles.Add(new Parcelle(new List<Case>() { new Case(false, false, false, false), new Case(false, false, false, false) }, 0));
            parcelles.Add(new Parcelle(new List<Case>() { new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false) }, 1));
            parcelles.Add(new Parcelle(new List<Case>() { new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false) }, 2));
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.MeilleureParcelle(parcelles), 2);
            Case case1 = new Case(true, true, true, true);
            parcelles.Add(new Parcelle(new List<Case>() { new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), case1, new Case(false, false, false, false) }, 3));
            parcelles.Add(new Parcelle(new List<Case>() { new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false), new Case(false, false, false, false) }, 4));
            case1.setAppartenance(Case.Appartenance.Ennemi);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.MeilleureParcelle(parcelles), 4);
        }

        [TestMethod]
        public void TestGetCasesJouablesAdjacentes()
        {

            m_carte.GetCases()[2, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[2, 3].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[3, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[6, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[2, 3].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[3, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 1].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 7].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[7, 8].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[9, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);
            m_carte.GetCases()[9, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Allie);

            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 6, 3 });
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouablesAdjacentes(m_carte);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouablesAdjacentes.Count, 6);

            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 8, 6 });
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouablesAdjacentes(m_carte);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouablesAdjacentes.Count, 2);
        }

        [TestMethod]
        public void TestZeroGraineAdjacente()
        {
            m_carte.GetCases()[2, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[2, 3].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[3, 2].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[6, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 4].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[7, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 5].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            m_carte.GetCases()[8, 6].setAppartenance(CoffeeChampionshipEngine.Case.Appartenance.Personne);
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 4, 3 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ZeroGraineAdjacente(m_carte)[0], 1);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ZeroGraineAdjacente(m_carte)[1], 3);
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 3, 4 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ZeroGraineAdjacente(m_carte)[0], 8);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ZeroGraineAdjacente(m_carte)[1], 4);
        }

        public void TestVerifNord()
        {
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifNord(0, 2, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifNord(8, 4, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifNord(3, 3, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifNord(7, 7, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(m_listeCasesAllieesNonComptees.Count, 7);
        }

        [TestMethod]
        public void TestVerifEst()
        {
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifEst(4, 9, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifEst(3, 1, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifEst(6, 3, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifEst(7, 7, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(m_listeCasesAllieesNonComptees.Count, 7);
        }

        [TestMethod]
        public void TestVerifSud()
        {
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifSud(0, 2, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifSud(5, 4, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifSud(1, 3, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifSud(7, 7, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(m_listeCasesAllieesNonComptees.Count, 7);
        }

        [TestMethod]
        public void TestVerifOuest()
        {
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifOuest(9, 2, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifOuest(3, 3, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifOuest(6, 5, m_listeCasesAllieesNonComptees), true);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.VerifOuest(7, 9, m_listeCasesAllieesNonComptees), false);
            Assert.AreEqual(m_listeCasesAllieesNonComptees.Count, 7);
        }

        [TestMethod]
        public void TestComptageChaines()
        {
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 5, 3 });
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouablesAdjacentes(m_carte);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouablesAdjacentes.Count, 3);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 12);

            CoffeeChampionshipEngine.IntelligenceArtificielle.ComptageChaines(m_carte);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines.Count, 3);

            List<int> v_longueurChaines = new List<int>();
            foreach (var v_case in CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines)
            {
                v_longueurChaines.Add(v_case.Value);
            }

            Assert.AreEqual(v_longueurChaines[0], 4);
            Assert.AreEqual(v_longueurChaines[1], 4);
            Assert.AreEqual(v_longueurChaines[2], 7);
        }
        [TestMethod]
        public void TestAjouterChaine()
        {
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouables(m_carte, new int[] { 5, 3 });
            CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesJouablesAdjacentes(m_carte);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouablesAdjacentes.Count, 3);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesJouables.Count, 12);

            int[] v_case = new int[] { 3, 5 };

            CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines.Add(v_case, 0);

            CoffeeChampionshipEngine.IntelligenceArtificielle.AjouterChaine(3, 3, m_listeCasesAllieesNonComptees, v_case);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines[v_case], 4);

            CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines.Clear();
            CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines.Add(v_case, 0);

            CoffeeChampionshipEngine.IntelligenceArtificielle.AjouterChaine(7, 3, m_listeCasesAllieesNonComptees, v_case);

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.CasesChaines[v_case], 7);
        }

        [TestMethod]
        public void TestGetCasesVoisinesLibres()
        {

            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesVoisinesLibres(new int[] { 1, 3 }, m_carte), 0);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesVoisinesLibres(new int[] { 3, 3 }, m_carte), 2);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesVoisinesLibres(new int[] { 7, 3 }, m_carte), 2);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.GetCasesVoisinesLibres(new int[] { 6, 2 }, m_carte), 3);

        }

        [TestMethod]
        public void TestChoixVoisinage()
        {
            List<int[]> v_placements = new List<int[]>();
            v_placements.Add(new int[] { 1, 3 });
            v_placements.Add(new int[] { 3, 3 });
            v_placements.Add(new int[] { 7, 3 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[0], 3);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[1], 3);

            v_placements.Add(new int[] { 6, 2 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[0], 6);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[1], 2);
        }

        [TestMethod]
        public void TestChoixControle()
        {
            List<int[]> v_placements = new List<int[]>();
            v_placements.Add(new int[] { 1, 3 });
            v_placements.Add(new int[] { 3, 3 });
            v_placements.Add(new int[] { 7, 3 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[0], 3);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[1], 3);

            v_placements.Add(new int[] { 1, 2 });
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[0], 3);
            Assert.AreEqual(CoffeeChampionshipEngine.IntelligenceArtificielle.ChoixVoisinage(v_placements, m_carte)[1], 3);

        }
    }
}
