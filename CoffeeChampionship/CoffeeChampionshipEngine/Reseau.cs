﻿using System;
using System.Net.Sockets;
using System.Text;

namespace CoffeeChampionshipEngine
{
    public class Reseau
    {
        private Socket serveur;

        //Constructeur: initialise le socket
        public Reseau(string serveur_ip, int serveur_port)
        {
            serveur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serveur.Connect(serveur_ip, serveur_port);
        }

        //Destructeur: termine la connexion du socket
        ~Reseau()
        {
            serveur.Shutdown(SocketShutdown.Both);
            serveur.Close();
        }

        //isNotZero: sert de "filtre" afin de supprimer les 0 lors de la réception
        //avec l'option estLong activée
        private bool isNotZero(byte value)
        {
            return value != 0;
        }

        //Recevoir: reçoit un message de 4 octets (ou plus si spécifié en paramètre),
        //le convertit en texte exploitable et le renvoie
        public string Recevoir(bool estLong = false)
        {
            byte[] v_donnees4 = new byte[4];
            byte[] v_donnees512 = new byte[512];
            byte[] v_donnees = new byte[0];

            if(estLong)
            {
                v_donnees = v_donnees512;
            }
            else
            {
                v_donnees = v_donnees4;
            }

            //reception des donnees
            serveur.Receive(v_donnees);
            //"annulation" du surdimensionnement du buffer
            v_donnees = Array.FindAll(v_donnees, isNotZero);

            return Decoder(v_donnees);
        }

        //Envoyer: code un string puis l'envoie au serveur
        public void Envoyer(string p_donnees)
        {
            byte[] v_donneesCodees = Coder(p_donnees);
            serveur.Send(v_donneesCodees);
        }

        //Decoder: transforme un tableau de byte en string exploitable
        private string Decoder(byte[] p_donnees)
        {
            return Encoding.UTF8.GetString(p_donnees, 0, p_donnees.Length);
        }

        //Coder: transforme un string en tableau de byte envoyable par le réseau
        private byte[] Coder(string p_donnees)
        {
            char[] v_donneesEnChar = p_donnees.ToCharArray();
            return Encoding.UTF8.GetBytes(v_donneesEnChar, 0, v_donneesEnChar.Length);
        }
    }
}
