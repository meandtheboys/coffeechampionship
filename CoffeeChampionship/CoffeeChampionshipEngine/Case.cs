﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeChampionshipEngine
{
    public class Case
    {
        public enum Terrain
        {
            Neutre,
            Mer,
            Foret
        }

        public enum Appartenance
        {
            Personne,
            Allie,
            Ennemi
        }

        private Terrain type_Terrain;
        private bool nord, est, ouest, sud;
        private int id_Parcelle;

        private Appartenance appartenance;

        public Case(bool nord, bool est, bool ouest, bool sud)
        {
            this.nord = nord;
            this.est = est;
            this.ouest = ouest;
            this.sud = sud;
            id_Parcelle = -1;
            appartenance = Appartenance.Personne;
        }

        public bool getNord()
        {
            return nord;
        }

        public bool getEst()
        {
            return est;
        }
        public bool getSud()
        {
            return sud;
        }
        public bool getOuest()
        {
            return ouest;
        }

        public Terrain getTerrain()
        {
            return type_Terrain;
        }

        public void setTerrain(Terrain terrain)
        {
            this.type_Terrain = terrain;
        }

        public Appartenance getAppartenance()
        {
            return appartenance;
        }

        public void setAppartenance(Appartenance appartenance)
        {
            this.appartenance = appartenance;
        }

        public int getIdParcelle()
        {
            return id_Parcelle;
        }

        public void setIdParcelle(int id_Parcelle)
        {
            this.id_Parcelle = id_Parcelle;
        }
    }
}
