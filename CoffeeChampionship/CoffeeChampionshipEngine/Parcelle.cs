﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CoffeeChampionshipEngine
{
    public class Parcelle
    {
        private int id_Parcelle;
        private List<Case> blocs = new List<Case>();


        public Parcelle(List<Case> cases, int id)
        {
            id_Parcelle = id;
            foreach (Case cellule in cases)
            {
                blocs.Add(cellule);
                cellule.setIdParcelle(id);
            }
        }

        public int getIdParcelle()
        {
            return id_Parcelle;
        }

        public List<Case> getBlocs()
        {
            return blocs;
        }

        public int getNbPersonneCase()
        {
            int nombre_case_dispo = 0;
            foreach (Case cellule in blocs)
            {
                if(cellule.getAppartenance() == Case.Appartenance.Personne)
                {
                    nombre_case_dispo++;
                }
            }
            return nombre_case_dispo;
        }

        //getPourcentageControle : Permet de connaitre le pourcentage de contrôle de la parcelle
        //OUT :
        //  float : pourcentage de controle de la parcelle.
        public float getPourcentageControle()
        {
            float nombre_case_allie = 0;
            foreach (Case cellule in blocs)
            {
                if (cellule.getAppartenance() == Case.Appartenance.Allie)
                {
                    nombre_case_allie = nombre_case_allie + 1;
                }
            }
            return nombre_case_allie / blocs.Count;
        }
    }
}
