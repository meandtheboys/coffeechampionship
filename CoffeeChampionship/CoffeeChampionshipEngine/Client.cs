﻿using System;

namespace CoffeeChampionshipEngine
{
    public class Client
    {
        private Reseau ioReseau;
        private Carte carte;

        private const String serveur_ip = "127.0.0.1";
        private const int serveur_port = 1213;

        //Constructeur: instancie le Reseau, la Carte puis lance la partie
        public Client()
        {
            ioReseau = new Reseau(serveur_ip, serveur_port);
            Console.WriteLine("[Carte] Réception");
            string data = ioReseau.Recevoir(true);
            Console.WriteLine("[Carte] Reçue");
            carte = new Carte(data);
            Console.WriteLine("[Carte] Instanciée");

            Console.WriteLine("Lancement de la partie");
            Partie();
        }

        //Partie: joue la partie à partir du premier coup du joueur
        private void Partie()
        {
            string messageRecu = "";
            int[] coup = { 0, 0 };
            bool premierTour = true;

            do
            {
                Console.WriteLine("[IA] Génère coup");
                coup = IntelligenceArtificielle.Jouer(carte, premierTour);
                Console.WriteLine("[Réseau] Envoi coup A:{0}{1}", coup[0], coup[1]);
                ioReseau.Envoyer(String.Format("A:{0}{1}", coup[0], coup[1]));
                carte.GetCases()[coup[0], coup[1]].setAppartenance(Case.Appartenance.Allie);

                if (premierTour)
                    premierTour = false;

                messageRecu = ioReseau.Recevoir();
                Console.WriteLine(String.Format("[Réseau] Réception : {0}", messageRecu));
                if(messageRecu == "INVA")
                {
                    Console.WriteLine("[Partie] Coup invalide");
                    carte.GetCases()[coup[0], coup[1]].setAppartenance(Case.Appartenance.Personne);
                }

                messageRecu = ioReseau.Recevoir();
                Console.WriteLine(String.Format("[Réseau] Réception : {0}", messageRecu));
                if(messageRecu != "FINI")
                {
                    Console.WriteLine("[Partie] Application du coup serveur");
                    coup[0] = int.Parse(messageRecu.Substring(2, 1));
                    coup[1] = int.Parse(messageRecu.Substring(3, 1));
                    carte.GetCases()[coup[0], coup[1]].setAppartenance(Case.Appartenance.Ennemi);
                    carte.DernierCoupJoue = new int[] { coup[0], coup[1] };

                    messageRecu = ioReseau.Recevoir();
                    Console.WriteLine(String.Format("[Réseau] Réception : {0}", messageRecu));
                }
            }
            while(messageRecu != "FINI");
            Console.WriteLine("[Partie] Fin");

            messageRecu = ioReseau.Recevoir(true);
            coup[0] = int.Parse(messageRecu.Substring(2, 2));
            coup[1] = int.Parse(messageRecu.Substring(5, 2));
            Console.WriteLine(String.Format("Client: {0} serveur: {1}", coup[0], coup[1]));
        }

        public Carte GetCarte()
        {
            return this.carte;
        }
    }
}
