﻿using System;
using System.Collections.Generic;
using System.Net;
using System.IO.Pipes;
using System.Text;

namespace CoffeeChampionshipEngine
{
    public static class IntelligenceArtificielle
    {
        private const int TAILLE_CARTE = 10;

        private static List<int[]> m_casesJouables = new List<int[]>();
        private static List<int[]> m_casesJouablesAdjacentes = new List<int[]>();

        public static List<int[]> CasesJouables { get => m_casesJouables; }
        public static List<int[]> CasesJouablesAdjacentes { get => m_casesJouablesAdjacentes; }

        private static Dictionary<int[], int> m_casesChaines = new Dictionary<int[], int>();

        public static Dictionary<int[], int> CasesChaines { get => m_casesChaines; set => m_casesChaines = value; }

        //Jouer : Joue un tour en appelant les bonnes fonctions selon si c'est le premier tour ou non.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu
        //  p_premier_tour : booleen égal à vrai si c'est le premier tour et faux sinon.
        //OUT :
        //  int[] : coordonnées de la case où l'on joue notre graine.
        public static int[] Jouer(Carte p_carte, bool p_premier_tour = false)
        {
            if (p_premier_tour)
            {
                GetCasesJouablesPremierTour(p_carte);
                return PremierTour(p_carte);
            }
            else
            {
                return PlacementStrategique(p_carte);
            }
        }

        //GetCasesJouables : Parcourt la ligne et colonne de la carte correspondant aux coordonnées de la dernière graine placée
        //et sauvegarde les coordonnées des cases n'ayant pas de graine et n'étant pas une mer ou une forêt.
        //Cette fonction permet ainsi de savoir quelle cases sont jouables, leur coordonnées étant stockées dans la liste m_casesDisponibles.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu
        //  p_poseX : coordonnée en X de la dernière graine placée.
        //  p_poseY : coordonnée en Y de la dernière graine placée.
        //OUT :
        //  Mise à jour de la liste de couple d'entier m_casesDisponibles contenant les coordonées de toutes les cases jouables ainsi que des listes
        //m_casesJouablesVerticale et m_casesJouablesHorizontale.
        public static void GetCasesJouables(Carte p_carte, int[] coordonneesDernierePose)
        {
            int p_poseI = coordonneesDernierePose[0], p_poseJ = coordonneesDernierePose[1];

            m_casesJouables.Clear();

            Case[,] v_casesCarte = p_carte.GetCases();

            //verification verticale
            for (int v_i = 0; v_i < 10; v_i++)
                if (v_casesCarte[v_i, p_poseJ].getTerrain() == Case.Terrain.Neutre &&
                        v_casesCarte[v_i, p_poseJ].getAppartenance() == Case.Appartenance.Personne &&
                             v_casesCarte[v_i, p_poseJ].getIdParcelle() != v_casesCarte[p_poseI, p_poseJ].getIdParcelle())
                {
                    m_casesJouables.Add(new int[] { v_i, p_poseJ });
                }

            //verification horizontale
            for (int v_j = 0; v_j < 10; v_j++)
                if (v_casesCarte[p_poseI, v_j].getTerrain() == Case.Terrain.Neutre &&
                        v_casesCarte[p_poseI, v_j].getAppartenance() == Case.Appartenance.Personne &&
                             v_casesCarte[p_poseI, v_j].getIdParcelle() != v_casesCarte[p_poseI, p_poseJ].getIdParcelle())
                {
                    m_casesJouables.Add(new int[] { p_poseI, v_j });
                }
        }

        //GetCasesJouablesPremierTour : Parcourt toutes les cases de la carte et sauvegarde les coordonnées des cases n'étant
        //pas une mer ou une forêt. Cette fonction permet ainsi de savoir quelle cases sont jouables au premier tour, leur 
        //coordonnées étant stockées dans la liste m_casesDisponibles.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu
        //OUT :
        //  Mise à jour de la liste de couple d'entier m_casesDisponibles contenant les coordonées de toutes les cases jouables.
        public static void GetCasesJouablesPremierTour(Carte p_carte)
        {
            m_casesJouables.Clear();
            Case[,] v_casesCarte = p_carte.GetCases();

            int v_coordX = 0, v_coordY = 0;

            foreach (Case v_cases in v_casesCarte)
            {
                if (v_cases.getTerrain() != Case.Terrain.Foret && v_cases.getTerrain() != Case.Terrain.Mer)
                    m_casesJouables.Add(new int[] { v_coordX, v_coordY });
                v_coordX++;
                if (v_coordX == 10)
                {
                    v_coordX = 0;
                    v_coordY++;
                }
            }
        }

        //CaseIdeal: Permet de determiner sur quelle case nous allons poser la graine.
        //           Pour le moment, nous utilisons juste un random.
        //OUT :
        //  Tableau d'entier contenant les cooordonnées de la case où nous allons poser la case.
        public static int[] CaseIdeale(Carte p_carte)  //paramètre avec un booléen premier tour
        {
            int[] v_coordonnees = new int[2];

            Random v_rnd = new Random();
            int v_nb_random = v_rnd.Next(0, m_casesJouables.Count);
            v_coordonnees[0] = m_casesJouables[v_nb_random][0];
            v_coordonnees[1] = m_casesJouables[v_nb_random][1];
            p_carte.SetCase(v_coordonnees[0], v_coordonnees[1], Case.Appartenance.Allie);

            return v_coordonnees;
        }

        //PremierTour: Permet de determiner sur quelle case nous allons poser la graine.
        //           On la pose sur la premiere parcelle de 6
        //IN :
        //  p_carte : tableau de Cases et de parcelles représentant la carte du jeu
        //OUT :
        //  Tableau d'entier contenant les cooordonnées de la case où nous allons poser la case.
        public static int[] PremierTour(Carte p_carte)
        {
            Case[,] v_casesCarte = p_carte.GetCases();
            List<Parcelle> v_parcellesCarte = p_carte.GetParcelles();

            foreach (int[] v_case in CasesJouables)
            {
                int id_parcelle = v_casesCarte[v_case[0], v_case[1]].getIdParcelle();
                if (v_parcellesCarte[id_parcelle].getNbPersonneCase() == 6)
                {
                    return v_case;
                }
            }

            return new int[2] { 0, 0 }; //On ne l'atteint jamais dans la théorie
        }

        //PlacementStrategique: Fonction qui fait le lien entre les trois cas possible de placement
        //                  0 : On place sur la parcelle avec le plus de case libre
        //                  1 : On place à côté de la seule graine adjacente existante
        //                  1+ : On place là où il y la plus grande chaine de graines
        //IN :
        //  p_graine_adjacentes : liste des coordonnées des graines adjacentes aux endroits de pose possible
        //OUT :
        //  Tableau d'entier contenant les cooordonnées de la case où nous allons poser la case.
        public static int[] PlacementStrategique(Carte p_carte)
        {
            GetCasesJouables(p_carte, p_carte.DernierCoupJoue);
            GetCasesJouablesAdjacentes(p_carte);

            int[] v_coordonnees = new int[2] { 0, 0 };

            if (m_casesJouablesAdjacentes.Count == 0)
            {
                return ZeroGraineAdjacente(p_carte);
            }
            if (m_casesJouablesAdjacentes.Count == 1)
            {
                return m_casesJouablesAdjacentes[0];
            }
            else
            {
                return ChoixPlusieursPlacement(p_carte);
            }
        }

        //ZeroGraineAdjacente: Fonction qui cherche la parcelle la plus profitable sur laquelle poser
        //                     On élabore d'abord une liste de parcelle
        //                     On appelle la fonction MeilleureParcelle pour savoir laquelle est la plus profitable
        //IN :
        //  p_carte : tableau de Cases et de parcelles représentant la carte du jeu
        //OUT :
        //  Tableau d'entier contenant les cooordonnées de la case où nous allons poser la case.
        public static int[] ZeroGraineAdjacente(Carte p_carte)
        {
            Case[,] v_casesCarte = p_carte.GetCases();
            List<Parcelle> v_parcellesCarte = p_carte.GetParcelles();
            List<Parcelle> v_parcelles = new List<Parcelle>();

            foreach (int[] cases in CasesJouables)
            {
                int id_parcelle = v_casesCarte[cases[0], cases[1]].getIdParcelle();
                v_parcelles.Add(v_parcellesCarte[id_parcelle]);
            }

            int v_meilleure_parcelle = MeilleureParcelle(v_parcelles);

            foreach (int[] cases in CasesJouables)
            {
                int id_parcelle = v_casesCarte[cases[0], cases[1]].getIdParcelle();
                if (id_parcelle == v_meilleure_parcelle)
                    return new int[2] { cases[0], cases[1] };
            }

            return new int[2] { 0, 0 }; //Techniquement on est jamais censé y aller.
        }

        //MeilleureParcelle: Fonction qui cherche la pacerelle la plus profitable sur laquelle poser (encore)
        //IN :
        //  p_parcelles : Liste des parcelles possible
        //OUT :
        //  L'id de la parcelle où il faut poser
        public static int MeilleureParcelle(List<Parcelle> p_parcelles)
        {
            Parcelle v_meilleure_parcelle = null;
            int v_case_vide = 0;
            foreach (Parcelle parcelle in p_parcelles)
            {
                if (parcelle.getNbPersonneCase() > v_case_vide)
                {
                    v_meilleure_parcelle = parcelle;
                    v_case_vide = parcelle.getNbPersonneCase();
                }
            }
            return v_meilleure_parcelle.getIdParcelle();
        }

        //GetCasesJouablesAdjacentes : Parcours les cases jouables et sauvegarde celles qui sont adjacentes à une case
        //où l'on a déjà posé une graine.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  Mise à jour de la liste de couple d'entier m_casesJouablesAdjacentes contenant les coordonées de toutes les cases jouables 
        //adjacentes à une case que nous contrôlons.
        public static void GetCasesJouablesAdjacentes(Carte p_carte)
        {
            Case[,] v_casesCarte = p_carte.GetCases();

            m_casesJouablesAdjacentes.Clear();

            foreach (int[] v_coordCaseJouable in m_casesJouables)
            {
                bool v_adjacente = false;

                if (v_coordCaseJouable[0] != 0)
                    if (v_casesCarte[v_coordCaseJouable[0] -1, v_coordCaseJouable[1]].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Allie)
                        v_adjacente = true;
                if (v_coordCaseJouable[0] != 9)
                    if (v_casesCarte[v_coordCaseJouable[0] + 1, v_coordCaseJouable[1]].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Allie)
                        v_adjacente = true;
                if (v_coordCaseJouable[1] != 0)
                    if (v_casesCarte[v_coordCaseJouable[0], v_coordCaseJouable[1] - 1].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Allie)
                        v_adjacente = true;
                if (v_coordCaseJouable[1] != 9)
                    if (v_casesCarte[v_coordCaseJouable[0], v_coordCaseJouable[1] + 1].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Allie)
                        v_adjacente = true;

                if (v_adjacente)
                    m_casesJouablesAdjacentes.Add(v_coordCaseJouable);
            }
        }

        //ComptageChaines : Permet de compter les longueur des différentes chaines pouvant être réalisées aux différents placements
        //possibles. Fait appelle à la fonction récursive AjouterChaine pour chaque case pour mettre à jour la liste m_casesChaines.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  Mise à jour de la liste du dictionnaire m_casesChaines associant les cases jouables avec les longueurs des
        //  chaines réalisables.
        public static void ComptageChaines(CoffeeChampionshipEngine.Carte p_carte)
        {
            CoffeeChampionshipEngine.Case[,] v_casesCarte = p_carte.GetCases();

            m_casesChaines.Clear();

            foreach (int[] v_caseJouable in m_casesJouablesAdjacentes)
            {
                //Liste des cases alliées, utilisée pour identifier chaque case de manière unique.
                List<int[]> v_casesAllieesNonComptees = new List<int[]>();
                for (int v_indiceLigne = 0; v_indiceLigne < TAILLE_CARTE; v_indiceLigne++)
                    for (int v_indiceColonne = 0; v_indiceColonne < TAILLE_CARTE; v_indiceColonne++)
                        if (v_casesCarte[v_indiceLigne, v_indiceColonne].getAppartenance() == Case.Appartenance.Allie)
                            v_casesAllieesNonComptees.Add(new int[] { v_indiceLigne, v_indiceColonne });

                m_casesChaines.Add(v_caseJouable, 0);
                AjouterChaine(v_caseJouable[0], v_caseJouable[1],v_casesAllieesNonComptees, v_caseJouable);
            }
        }

        //AjouterChaine : Fonction récursive permettant de compter la longueur de la chaine pouvant être réaliser en
        // plaçant une graine au coordonnées passées en paramettre.
        //IN : 
        //  p_indiceColonne : Indice de la colonne de la case à tester.
        //  p_indiceLigne : Indice de la Ligne de la case à tester.
        //  p_casesAllieesNonComptees : liste des cases contenant une de nos graines pas encore prise en compte.
        //  p_caseJouable : coordonnées de la case jouable à partir de laquelle on commence à compter la chaine.
        //OUT :
        //  Mise à jour de la liste du dictionnaire m_casesChaines associant les cases jouables avec les longueurs des
        //  chaines réalisables.
        public static void AjouterChaine(int p_indiceLigne, int p_indiceColonne, List<int[]> p_casesAllieesNonComptees, int[] p_caseJouable)
        {
            m_casesChaines[p_caseJouable] ++;

            if (VerifNord(p_indiceLigne, p_indiceColonne, p_casesAllieesNonComptees))
                AjouterChaine(p_indiceLigne - 1, p_indiceColonne, p_casesAllieesNonComptees, p_caseJouable);

            if (VerifEst(p_indiceLigne, p_indiceColonne, p_casesAllieesNonComptees))
                AjouterChaine(p_indiceLigne, p_indiceColonne + 1, p_casesAllieesNonComptees, p_caseJouable);

            if (VerifOuest(p_indiceLigne, p_indiceColonne, p_casesAllieesNonComptees))
                AjouterChaine(p_indiceLigne, p_indiceColonne - 1, p_casesAllieesNonComptees, p_caseJouable);

            if (VerifSud(p_indiceLigne, p_indiceColonne, p_casesAllieesNonComptees))
                AjouterChaine(p_indiceLigne + 1, p_indiceColonne, p_casesAllieesNonComptees, p_caseJouable);
        }

        //VerfifNord : Fonction de vérification qui permet de savoir si la case au nord de la case dont les coordonnées
        //sont passées en paramètre est une case contenant une de nos graines. Pour cela on regarde dans la liste de nos cases
        //si la case située aux coordonnées au dessus de celle à vérifier en fait partie et si on la trouve on la retire de la
        //liste pour ne pas qu'elle soit comptée deux fois (problème avec la récursivité sinon).
        //IN : 
        //  p_indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  p_indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //  p_casesAllieesNonComptees : liste des cases contenant une de nos graines pas encore prise en compte.
        //OUT :
        //  boolean : égal à vrai si la case au nord de celle d'indice p_indiceLigne et p_indiceColonne en une case
        //            contenant une de nos graines ou faux si ce n'est pas le cas.
        public static bool VerifNord(int p_indiceLigne, int p_indiceColonne, List<int[]> p_casesAllieesNonComptees)
        {
            int v_indiceListe = 0;
            foreach (int[] v_coordCase in p_casesAllieesNonComptees)
            {
                if (v_coordCase[0] == p_indiceLigne - 1 && v_coordCase[1] == p_indiceColonne)
                {
                    p_casesAllieesNonComptees.RemoveAt(v_indiceListe);
                    return true;
                }
                v_indiceListe++;
            }
            return false;
        }

        //VerifEst : Fonction de vérification qui permet de savoir si la case à l'est de la case dont les coordonnées
        //sont passées en paramètre est une case contenant une de nos graines. Pour cela on regarde dans la liste de nos cases
        //si la case située aux coordonnées au dessus de celle à vérifier en fait partie et si on la trouve on la retire de la
        //liste pour ne pas qu'elle soit comptée deux fois (problème avec la récursivité sinon).
        //IN : 
        //  p_indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  p_indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //  p_casesAllieesNonComptees : liste des cases contenant une de nos graines pas encore prise en compte.
        //OUT :
        //  boolean : égal à vrai si la case à l'est de celle d'indice p_indiceLigne et p_indiceColonne en une case
        //            contenant une de nos graines ou faux si ce n'est pas le cas.
        public static bool VerifEst(int p_indiceLigne, int p_indiceColonne, List<int[]> p_casesAllieesNonComptees)
        {
            int v_indiceListe = 0;
            foreach (int[] v_coordCase in p_casesAllieesNonComptees)
            {
                if (v_coordCase[0] == p_indiceLigne && v_coordCase[1] == p_indiceColonne + 1)
                {
                    p_casesAllieesNonComptees.RemoveAt(v_indiceListe);
                    return true;
                }
                v_indiceListe++;
            }
            return false;
        }

        //VerifSud : Fonction de vérification qui permet de savoir si la case au sud de la case dont les coordonnées
        //sont passées en paramètre est une case contenant une de nos graines. Pour cela on regarde dans la liste de nos cases
        //si la case située aux coordonnées au dessus de celle à vérifier en fait partie et si on la trouve on la retire de la
        //liste pour ne pas qu'elle soit comptée deux fois (problème avec la récursivité sinon).
        //IN : 
        //  p_indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  p_indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //  p_casesAllieesNonComptees : liste des cases contenant une de nos graines pas encore prise en compte.
        //OUT :
        //  boolean : égal à vrai si la case au sud de celle d'indice p_indiceLigne et p_indiceColonne en une case
        //            contenant une de nos graines ou faux si ce n'est pas le cas.
        public static bool VerifSud(int p_indiceLigne, int p_indiceColonne, List<int[]> p_casesAllieesNonComptees)
        {
                int v_indiceListe = 0;
                foreach (int[] v_coordCase in p_casesAllieesNonComptees)
                {
                    if (v_coordCase[0] == p_indiceLigne + 1 && v_coordCase[1] == p_indiceColonne)
                    {
                        p_casesAllieesNonComptees.RemoveAt(v_indiceListe);
                        return true;
                    }
                    v_indiceListe++;
                }
            return false;
        }

        //VerifOuest : Fonction de vérification qui permet de savoir si la case à l'ouest de la case dont les coordonnées
        //sont passées en paramètre est une case contenant une de nos graines. Pour cela on regarde dans la liste de nos cases
        //si la case située aux coordonnées au dessus de celle à vérifier en fait partie et si on la trouve on la retire de la
        //liste pour ne pas qu'elle soit comptée deux fois (problème avec la récursivité sinon).
        //IN : 
        //  p_indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  p_indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //  p_casesAllieesNonComptees : liste des cases contenant une de nos graines pas encore prise en compte.
        //OUT :
        //  boolean : égal à vrai si la case à l'ouest de celle d'indice p_indiceLigne et p_indiceColonne en une case
        //            contenant une de nos graines ou faux si ce n'est pas le cas.
        public static bool VerifOuest(int p_indiceLigne, int p_indiceColonne, List<int[]> p_casesAllieesNonComptees)
        {
            int v_indiceListe = 0;
            foreach (int[] v_coordCase in p_casesAllieesNonComptees)
            {
                if (v_coordCase[0] == p_indiceLigne && v_coordCase[1] == p_indiceColonne - 1)
                {
                    p_casesAllieesNonComptees.RemoveAt(v_indiceListe);
                    return true;
                }
                v_indiceListe++;
            }
            return false;
        }

        //ChoixPlusieursPlacement : permet de choisir la case jouable reliée à la plus grande chaine de graine que l'on peut
        //réalisée. Si plusieurs cases sont possibles, on appelle la fonction ChoixControle qui determinera la quelle est la 
        //meilleure en fonction du controle des parcelles.
        //IN : 
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  int[] : coordonnées de la case à jouer.
        public static int[] ChoixPlusieursPlacement(CoffeeChampionshipEngine.Carte p_carte)
        {
            ComptageChaines(p_carte);

            List<int[]> v_meilleurPlacement = new List<int[]>();

             int v_longueurMax = 0;

            foreach(var v_case in m_casesChaines)
            {
                if (v_case.Value > v_longueurMax)
                    v_longueurMax = v_case.Value;
            }

            foreach (var v_case in m_casesChaines)
            {
                if (v_case.Value == v_longueurMax)
                    v_meilleurPlacement.Add(v_case.Key);
                    
            }

            if (v_meilleurPlacement.Count > 1)
            {
                return ChoixControle(v_meilleurPlacement, p_carte);
            }

            return v_meilleurPlacement[0];
        }

        //ChoixControle : permet de choisir la case à jouer parmi la liste de coordonnées passée en paramètre de
        //manière à privilégier les parcelles encore non controlées mais avec le plus grand niveau de contrôle.
        //en cas de choix multiple, on appelle la fonction ChoixVoisinage qui determinera la case en fonction
        //des cases voisines de celles à considérer.
        //IN : 
        //  p_meilleurPlacement : liste des coordonnées des cases à considérer.
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  int[] : coordonnées de la case à jouer.
        public static int[] ChoixControle(List<int[]> p_meilleurPlacement, CoffeeChampionshipEngine.Carte p_carte)
        {
            Dictionary<int[], float> v_placementPourcentageControle = new Dictionary<int[], float>();
            float v_controleMax = 0;

            foreach (int[] v_placement in p_meilleurPlacement)
            {
                foreach (CoffeeChampionshipEngine.Parcelle v_parcelle in p_carte.GetParcelles())
                {
                    if (p_carte.GetCases()[v_placement[0], v_placement[1]].getIdParcelle() == v_parcelle.getIdParcelle() && v_parcelle.getPourcentageControle() <= 0.5)
                    {
                        v_placementPourcentageControle.Add(v_placement, v_parcelle.getPourcentageControle());
                        if (v_controleMax < v_parcelle.getPourcentageControle())
                            v_controleMax = v_parcelle.getPourcentageControle();
                    }
                }
            }

            List<int[]> v_meilleuresCases = new List<int[]>();
            foreach (var v_placement in v_placementPourcentageControle)
            {
                if (v_placement.Value == v_controleMax)
                    v_meilleuresCases.Add(v_placement.Key);
            }

            if (v_meilleuresCases.Count == 0)
                return ChoixVoisinage(p_meilleurPlacement, p_carte);
            else
                if (v_meilleuresCases.Count > 1)
                    return ChoixVoisinage(v_meilleuresCases, p_carte);
            return v_meilleuresCases[0];
        }

        //ChoixVoisinage : permet de choisir la case à jouer parmis la liste de coordonnées passée en paramètre de
        //manière à privilégier les cases ayant des cases voisines vide et faisant partie d'une parcelle différentes.
        //Si des éqgalité subsiste, on renvoie par défaut la première case de la liste obtenue après tri.
        //NOTE : il est possible d'utiliser directement cette stratégie depuis la fonction ChoixPlusieursPlacement à la
        //place le ChoixControle.
        //IN : 
        //  p_meilleurPlacement : liste des coordonnées des cases à considérer.
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  int[] : coordonnées de la case à jouer.
        public static int[] ChoixVoisinage(List<int[]> p_meilleurPlacement, CoffeeChampionshipEngine.Carte p_carte)
        {
            Dictionary<int[], int> v_placementVoisinage = new Dictionary<int[], int>();
            int v_voisinageMax = 0;

            foreach (int[] v_placement in p_meilleurPlacement)
            {
                v_placementVoisinage.Add(v_placement, GetCasesVoisinesLibres(v_placement, p_carte));
                if (GetCasesVoisinesLibres(v_placement, p_carte) > v_voisinageMax)
                    v_voisinageMax = GetCasesVoisinesLibres(v_placement, p_carte);
            }

            List<int[]> v_meilleuresCases = new List<int[]>();
            foreach (var v_placement in v_placementVoisinage)
            {
                if (v_placement.Value == v_voisinageMax)
                    v_meilleuresCases.Add(v_placement.Key);
            }

            return v_meilleuresCases[0];
        }

        //GetCasesVoisinesLibres : Compte le nombre de cases voisines non occupées, jouables et ne faisant pas
        //partie de la même parcelle que la case dont les coordonnées sont passées en paramètre.
        //IN : 
        //  p_coords : coordonnées de la case à tester.
        //  p_carte : tableau de Cases représentant la carte du jeu.
        //OUT :
        //  int : nombre de cases voisines.
        public static int GetCasesVoisinesLibres(int[] p_coords, CoffeeChampionshipEngine.Carte p_carte)
        {
            int v_compteur = 0;
            if (p_coords[0] > 0)
                if (p_carte.GetCases()[p_coords[0], p_coords[1]].getIdParcelle() != p_carte.GetCases()[p_coords[0] - 1, p_coords[1]].getIdParcelle()
                        && p_carte.GetCases()[p_coords[0] - 1, p_coords[1]].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Personne
                            && p_carte.GetCases()[p_coords[0] - 1, p_coords[1]].getTerrain() == CoffeeChampionshipEngine.Case.Terrain.Neutre)
                    v_compteur++;
            if (p_coords[0] < TAILLE_CARTE - 1)
                if (p_carte.GetCases()[p_coords[0], p_coords[1]].getIdParcelle() != p_carte.GetCases()[p_coords[0] + 1, p_coords[1]].getIdParcelle()
                        && p_carte.GetCases()[p_coords[0] + 1, p_coords[1]].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Personne
                            && p_carte.GetCases()[p_coords[0] + 1, p_coords[1]].getTerrain() == CoffeeChampionshipEngine.Case.Terrain.Neutre)
                    v_compteur++;
            if (p_coords[1] > 0)
                if (p_carte.GetCases()[p_coords[0], p_coords[1]].getIdParcelle() != p_carte.GetCases()[p_coords[0], p_coords[1] - 1].getIdParcelle()
                        && p_carte.GetCases()[p_coords[0], p_coords[1] - 1].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Personne
                            && p_carte.GetCases()[p_coords[0], p_coords[1] - 1].getTerrain() == CoffeeChampionshipEngine.Case.Terrain.Neutre)
                    v_compteur++;
            if (p_coords[1] < TAILLE_CARTE - 1)
                if (p_carte.GetCases()[p_coords[0], p_coords[1]].getIdParcelle() != p_carte.GetCases()[p_coords[0], p_coords[1] + 1].getIdParcelle()
                        && p_carte.GetCases()[p_coords[0], p_coords[1] + 1].getAppartenance() == CoffeeChampionshipEngine.Case.Appartenance.Personne
                            && p_carte.GetCases()[p_coords[0], p_coords[1] + 1].getTerrain() == CoffeeChampionshipEngine.Case.Terrain.Neutre)
                    v_compteur++;
            return v_compteur;
        }
    }
}