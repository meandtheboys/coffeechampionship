﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeChampionshipEngine
{
    public class Carte
    {
        private List<Parcelle> parcelles;
        private Case[,] cases;
        private int[] dernier_coup_joue = new int[2];

        public Carte(string data)
        {
            SetCarte(data);
        }

        //Traduire : Découpe la chaine de caractère correspondant à la donnée décodée du serveur en Entier.
        //Stock les nouvelles données dans un tableau de 10x10 cases et retourne un tableau de type Case
        //IN : 
        //  decodedData : Donnée du serveur décodée par la fonction Decoder. Elle informe sur la position des frontière et le type de la case
        //OUT :
        //  convertionIntEnCase(terrain) : tableau de type Case. Chaque case informe sur les frontières et le type qu'elle a.
        public static Case[,] Traduire(string decodedData)
        {
            int[,] terrain = new int[10,10];
            int x = 0, y = 0;
            string[] lignes = decodedData.Split('|');

            for(x = 0; x < 10; x++)
            {
                y = 0;
                string[] unites = lignes[x].Split(':');
                foreach (string unite in unites)
                {
                    Int32.TryParse(unite, out terrain[x, y]);
                    y++;
                }
            }

            return ConvertionIntEnCase(terrain);
        }

        //ConvertionIntEnCase: Reçoit un tableau d'entier, le parcourt et pour chaque entier, créée un objet Case.
        //Stock l'objet Case dans un tableau de Case de 10x10. La fonction crée la case avec le type et les frontières qu'elle doit avoir selon l'entier donné.
        //IN : 
        //  terrain[10,10] : tableau d'entiers informant sur quelles frontières la Case possède et quel et son type
        //OUT :
        //  terrainCase : tableau de 10x10 de Case
        public static Case[,] ConvertionIntEnCase(int[,] terrain)
        {
            Case[,] terrainCase = new Case[10, 10];
            for(int y = 0; y<10; y++)
                for(int x = 0; x<10; x++)
                {
                    if ( terrain[x, y] / 64.0 >= 1)
                    {
                        terrainCase[x, y] = Frontiere(terrain[x, y] % 64);
                        terrainCase[x, y].setTerrain(Case.Terrain.Mer);
                    }
                    else
                    {
                        if (terrain[x, y] / 32.0 >= 1)
                        {
                            terrainCase[x, y] = Frontiere(terrain[x, y] % 32);
                            terrainCase[x, y].setTerrain(Case.Terrain.Foret);
                        }
                        else
                        {
                            terrainCase[x, y] = Frontiere(terrain[x, y]);
                            terrainCase[x, y].setTerrain(Case.Terrain.Neutre);
                        }
                    }
                }
            return terrainCase;
        }

        //Frontiere: Reçoit un entier et le divise par 8, 4, 2 et 1. Si l'entier divisé et supérieur ou égal à 1, on accorde une frontière selon le dividende.
        //IN : 
        //  nombre : Entier qui sera diviser par un dividende
        //OUT :
        //  Objet Case : avec les frontières qu'il possède
        public static Case Frontiere(int nombre)
        {
            bool[] tabFrontiere = new bool[4];      // est, sud, ouest, nord;
            int dividende = 8;
            for(int indice = 0; indice < 4; indice++)
            {
                if(nombre/dividende >= 1)
                {
                    tabFrontiere[indice] = true;
                    nombre -= dividende;
                }
                dividende /= 2;
            }
            return new Case(tabFrontiere[3], tabFrontiere[0], tabFrontiere[2], tabFrontiere[1]);
        }

        public void SetCarte(string data)
        {
            cases = Traduire(data);
            parcelles = CreationParcelle.CreerParcelles(cases);
        }

        public Case[,] GetCases()
        {
            return this.cases;
        }

        public List<Parcelle> GetParcelles()
        {
            return this.parcelles;
        }

        public int[] DernierCoupJoue { get => dernier_coup_joue; set => dernier_coup_joue = value; }

        public Case.Appartenance SetCase(int x, int y, Case.Appartenance appartenance)
        {
            this.cases[x, y].setAppartenance(appartenance);
            this.dernier_coup_joue = new int[2] { x, y };
            return this.cases[x, y].getAppartenance();
        }
    }
}
