﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeChampionshipEngine
{
    public static class CreationParcelle
    {
        private const int taille_Carte = 10;
        private static List<Case> CasesParcelles = new List<Case>();


        //CreerParcelles : Permet d'appeler autant de fois la fonction NouvelleParcelle qu'il n'y a de parcelle dans la carte
        //afin de créer toutes les parcelles du jeu. Les parcelles créées sont ajoutées et retournées dans une liste de parcelle. 
        //IN : 
        //  tab_Case : tableau de Case représentant la carte du jeu
        //OUT :
        //  parcelles : liste des parcelles créées
        public static List<Parcelle> CreerParcelles(Case[,] tab_Case)
        {
            List<Parcelle> parcelles = new List<Parcelle>();

            int id_Parcelle = 0;

            while (!ParcellesCrees(tab_Case))
            {
                parcelles.Add(NouvelleParcelle(tab_Case, id_Parcelle));
                id_Parcelle++;
            }

            return parcelles;
        }

        //CreationParcelle : Parcourt la carte représenter par un tableau de Case pour trouver la première case non assignée 
        //à une Parcelle et crée la Parcelle correspondante en appelant AjouterCase. Retourne la parcelle créée.
        //Cette fonction est à appeler autant de fois qu'il n'y a de Parcelles dans la carte pour instancier chacune d'entre elle.
        //IN : 
        //  tab_Case : tableau de Case représentant la carte du jeu
        //OUT :
        //  parcelle créée
        public static Parcelle NouvelleParcelle(Case[,] tab_Case, int id_Parcelle)
        {
            CasesParcelles.Clear();
            int indiceLigne = 0;
            int indiceColonne = 0;
            while (tab_Case[indiceLigne, indiceColonne].getIdParcelle() != -1)
            {
                indiceColonne++;
                if (indiceColonne == taille_Carte)
                {
                    indiceColonne = 0;
                    indiceLigne++;
                }
            }
            AjouterCase(tab_Case, indiceLigne, indiceColonne, id_Parcelle);
            return new Parcelle(CasesParcelles, id_Parcelle);
        }

        //AjouterCase : fonction récursive qui permet de regrouper toutes les cases d'une même parcelle 
        //dans la liste CasesParcelles.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu
        //  indiceColonne : Indice de la colonne dans tab_Case de la case à ajouter à la liste
        //  indiceLigne : Indice de la Ligne dans tab_Case de la case à ajouter à la liste
        //  id_Parcelle : id de la parcelle correspondante
        public static void AjouterCase(Case[,] tab_Case, int indiceLigne, int indiceColonne, int id_Parcelle)
        {
            CasesParcelles.Add(tab_Case[indiceLigne, indiceColonne]);
            tab_Case[indiceLigne, indiceColonne].setIdParcelle(id_Parcelle);

            if (VerifNord(tab_Case, indiceLigne, indiceColonne))
                AjouterCase(tab_Case, indiceLigne - 1, indiceColonne, id_Parcelle);

            if (VerifEst(tab_Case, indiceLigne, indiceColonne))
                AjouterCase(tab_Case, indiceLigne, indiceColonne + 1, id_Parcelle);

            if (VerifOuest(tab_Case, indiceLigne, indiceColonne))
                AjouterCase(tab_Case, indiceLigne, indiceColonne - 1, id_Parcelle);

            if (VerifSud(tab_Case, indiceLigne, indiceColonne))
                AjouterCase(tab_Case, indiceLigne + 1, indiceColonne, id_Parcelle);
        }

        //VerfifNord : Fonction de vérification qui permet de savoir si la case se situant aux coordonnées 
        //passées en paramètre appartient à la même parcelle que celle au nord de cette dernière.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu
        //  indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //OUT :
        //  boolean : égal à vrai si la case au nord de celle d'indice indiceLigne et indiceColonne fait partie de
        //            la même parcelle ou faux si ce n'est pas le cas.
        public static bool VerifNord(Case[,] tab_Case, int indiceLigne, int indiceColonne)
        {
            if (indiceLigne > 0)
                if (!tab_Case[indiceLigne, indiceColonne].getNord() && tab_Case[indiceLigne - 1, indiceColonne].getIdParcelle() == -1)
                    return true;
            return false;
        }

        //VerfifEst : Fonction de vérification qui permet de savoir si la case se situant aux coordonnées 
        //passées en paramètre appartient à la même parcelle que celle à l'est de cette dernière.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu
        //  indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //OUT :
        //  boolean : égal à vrai si la case à l'est de celle d'indice indiceLigne et indiceColonne fait partie de
        //            la même parcelle ou faux si ce n'est pas le cas.
        public static bool VerifEst(Case[,] tab_Case, int indiceLigne, int indiceColonne)
        {
            if (indiceColonne < taille_Carte - 1)
                if (!tab_Case[indiceLigne, indiceColonne].getEst() && tab_Case[indiceLigne, indiceColonne + 1].getIdParcelle() == -1)
                    return true;
            return false;
        }

        //VerfifSud : Fonction de vérification qui permet de savoir si la case se situant aux coordonnées 
        //passées en paramètre appartient à la même parcelle que celle au sud de cette dernière.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu
        //  indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //OUT :
        //  boolean : égal à vrai si la case au sud de celle d'indice indiceLigne et indiceColonne fait partie de
        //            la même parcelle ou faux si ce n'est pas le cas.
        public static bool VerifSud(Case[,] tab_Case, int indiceLigne, int indiceColonne)
        {
            if (indiceLigne < taille_Carte - 1)
                if (!tab_Case[indiceLigne, indiceColonne].getSud() && tab_Case[indiceLigne + 1, indiceColonne].getIdParcelle() == -1)
                    return true;
            return false;
        }

        //VerfifOuest : Fonction de vérification qui permet de savoir si la case se situant aux coordonnées 
        //passées en paramètre appartient à la même parcelle que celle à l'ouest de cette dernière.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu
        //  indiceColonne : Indice de la colonne dans tab_Case de la case à tester.
        //  indiceLigne : Indice de la Ligne dans tab_Case de la case à tester.
        //OUT :
        //  boolean : égal à vrai si la case à l'ouest de celle d'indice indiceLigne et indiceColonne fait partie de
        //            la même parcelle ou faux si ce n'est pas le cas.
        public static bool VerifOuest(Case[,] tab_Case, int indiceLigne, int indiceColonne)
        {
            if (indiceColonne > 0)
                if (!tab_Case[indiceLigne, indiceColonne].getOuest() && tab_Case[indiceLigne, indiceColonne - 1].getIdParcelle() == -1)
                    return true;
            return false;
        }

        //ParcellesCrees : Fonction de vérification qui permet de savoir si toutes les cases sont associées à une parcelle.
        //IN : 
        //  tab_Case : Tableau de Case représentant la carte du jeu.
        //OUT :
        //  boolean : égale à vrai si toutes les cases sont associées à une parcelle et 0 sinon.
        public static bool ParcellesCrees(Case[,] tab_Case)
        {
            foreach(CoffeeChampionshipEngine.Case Case in tab_Case)
            {
                if (Case.getIdParcelle() == -1)
                    return false;
            }
            return true;
        }
    }
}
